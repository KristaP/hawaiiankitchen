using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FoodTable : MonoBehaviour
{
    [SerializeField]
    private Recipe currentRecipe;
    [SerializeField]
    public int currentDifficulty = 1;

    public List<GameObject> recipePanels = new List<GameObject>();
    public GameObject currentPanel;
    public List<Recipe> recipes = new List<Recipe>();
    public static FoodTable Instance;

    public UnityEvent OnRecipeFailed;
    public UnityEvent OnTableCleared;

    public bool CombineIngrediences = false;

    //Setup
    public void Awake() {
        if(!Instance)
            Instance = this;
        currentRecipe = null;
        NewRecipes();
    }

    private void Start()
    {
        GameManager.Instance.OnStateChanged += OnStateChanged;
    }
    
    public void OnStateChanged()
    {
        if (GameManager.Instance.State != GameState.Evaluation)
            return;
            
        foreach (GameObject ingredient in currentRecipe.ItemsOnSandwich)
        {
            ingredient.transform.SetParent(gameObject.transform, true);
            ingredient.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.F)) {
            currentRecipe = new Recipe(currentDifficulty);
        }

        if(currentRecipe != null && GameManager.Instance.State == GameState.Game) {
            if(currentRecipe.CurrentModifier != null)
                if(!currentRecipe.CurrentModifier.ContinueRun(currentRecipe)){
                    OnRecipeFailed.Invoke();
                }
        }
    }

    //Generates a new recipe
    private void NewRecipes() {
        //Clear items on table
        ClearTable();

        foreach(GameObject panel in recipePanels){
            panel.SetActive(true);
            recipes.Add(new Recipe(currentDifficulty));
            panel.GetComponent<TextMeshProUGUI>().text = recipes.Last();
        }
        currentDifficulty++;
    }

    private void ClearTable(){
        if(currentRecipe != null)
            for(int x = 0; x < currentRecipe.ItemsOnSandwich.Count; x++)
                currentRecipe.ItemsOnSandwich[x].SetActive(false);
        recipes = new List<Recipe>();
        OnTableCleared?.Invoke();
    }

    public void SetRecipe(int index){
        currentRecipe = recipes[index];
        SetRecipe();
    }

    public void SetRecipe(Ingredience bread){
        currentRecipe = new Recipe(currentDifficulty, bread);
        SetRecipe();
    }

    public void SetRecipe(Recipe recipe){
        currentRecipe = recipe;
        SetRecipe();
    }

    public Recipe NewRecipe(){
        return new Recipe(currentDifficulty);
    }

    private void SetRecipe(){
        GameManager.Instance.State = GameState.Game;
        currentPanel.GetComponentInChildren<TextMeshProUGUI>().text = currentRecipe;
    }

    public float GetEvaluation()
    {
        return currentRecipe.CurrentModifier.Evaluation(currentRecipe);
    }

    //Add Item to table
    private void OnTriggerEnter(Collider other) {
        if(!TryGetComponent<MeshFilter>(out MeshFilter meshFilter) || other.tag == "IngredientStart" || currentRecipe == null)
            return;
        if(!meshFilter.sharedMesh || currentRecipe == "" || GameManager.Instance.State == GameState.Evaluation)
            return;
        currentRecipe.ItemsOnSandwich.Add(other.gameObject);
    }

    //Remove Item from table
    private void OnTriggerExit(Collider other) {
        if(!TryGetComponent<MeshFilter>(out MeshFilter meshFilter) || other.tag == "IngredientStart" || currentRecipe == null)
            return;
        if(!meshFilter.sharedMesh || currentRecipe == "" || GameManager.Instance.State == GameState.Evaluation)
            return;
        currentRecipe.ItemsOnSandwich.Remove(other.gameObject);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(FoodTable))]
[CanEditMultipleObjects]
public class FoodTableEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        base.DrawDefaultInspector();
        EditorGUILayout.HelpBox("Enabling this option will mix human food and alien food, and will treat them as one type.", MessageType.Info);
    }
}
#endif
