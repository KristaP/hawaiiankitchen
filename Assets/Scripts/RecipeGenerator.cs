using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RecipeGenerator : MonoBehaviour
{
    public TextMeshProUGUI textMesh;
    public Recipe recipe;

    private void Reset()
    {
        textMesh = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void OnEnable() {
        if(FoodTable.Instance){
            recipe = FoodTable.Instance.NewRecipe();
            textMesh.text = recipe;
        }
    }

    public void Select(){
        FoodTable.Instance.SetRecipe(recipe);
    }
}
