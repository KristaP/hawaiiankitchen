using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShowInGameState : MonoBehaviour
{
    public GameState state = GameState.Menu;
    public float fadeInDelay = 0f;
    public float fadeOutDelay = 0f;

    public UnityEvent OnShow;
    public UnityEvent OnHide;

    Vector3 startPosition;
    Quaternion startRotation;
    Vector3 startScale;

    public bool doesPopup = true;

    private void Start() {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startScale = transform.localScale;

        GameManager.Instance.OnStateChanged += OnStateChanged;
        OnStateChanged();
    }

    private void OnDestroy() {
        GameManager.Instance.OnStateChanged -= OnStateChanged;
    }

    private void OnStateChanged()
    {
        if (!enabled)
            return;
        if(state == GameManager.Instance.State && !gameObject.activeSelf)
            GameManager.Instance.StartCoroutine(ScaleUp());
        else if(state != GameManager.Instance.State && gameObject.activeSelf)
            GameManager.Instance.StartCoroutine(ScaleDown());
    }

    private IEnumerator ScaleUp(){
        gameObject.SetActive(false);
        transform.position = startPosition;
        transform.rotation = startRotation;
        yield return new WaitForSeconds(fadeInDelay);
        gameObject.SetActive(true);
        OnShow?.Invoke();
        if(doesPopup){
            transform.localScale = Vector3.zero;
            while(Mathf.Abs(transform.localScale.x - startScale.x) < 0.05f) {
                transform.localScale += startScale * Time.deltaTime * 10;
                yield return null;
            }
            transform.localScale = startScale;
        }
    }

    private IEnumerator ScaleDown(){
        yield return new WaitForSeconds(fadeOutDelay);
        if(Time.timeSinceLevelLoad > 1f)
            OnHide?.Invoke();
        if(doesPopup){
            while(Mathf.Abs(transform.localScale.x) > 0.05f) {
                transform.localScale *= 1 - Time.deltaTime * 10;
                yield return null;
            }
            transform.localScale = Vector3.zero;
        }
        gameObject.SetActive(false);
        transform.position = startPosition;
        transform.rotation = startRotation;
    }
}
