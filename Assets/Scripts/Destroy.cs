using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public void OnEnable()
    {
        if(Time.timeSinceLevelLoad > 3)
            gameObject.SetActive(false);
        else
            enabled = false;
    }
}
