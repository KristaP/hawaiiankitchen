using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimManager : MonoBehaviour
{

    Animator anim;

    string isGood = "isGood";
    string isOK = "isOK";
    string isBad = "isBad";
    
    bool hasTurned;


    void Start()
    {
        anim = GetComponent<Animator>();
        
        GameManager.Instance.OnEvaluation.AddListener(Evaluate);
    }

    void Evaluate(float score)
    {
        string state;
        if (score == 1f)
            state = isGood;
        else if (score >= 0.6f)
            state = isOK;
        else
            state = isBad;
        Debug.Log(state);
        anim.SetTrigger(state);
    }

}
