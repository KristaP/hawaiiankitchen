using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameState state;
    public GameState State
    {
        get { return state; }
        set
        {
            state = value;
            OnStateChanged.Invoke();
        }
    }
    public static GameManager Instance;
    public Action OnStateChanged;
    public UnityEvent<float> OnEvaluation;

    void Awake()
    {
        if(!Instance)
            Instance = this;
        else
            Destroy(this);
        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        State = GameState.Menu;
    }

    public void SetState(int index){
        State = (GameState)index;
    }

    public void Evaluate()
    {
        OnEvaluation.Invoke(FoodTable.Instance.GetEvaluation());
    }
}

public enum GameState
{
    Menu,
    ChooseRecipe,
    Game,
    Evaluation,
    GameOver,
    GameExit
}