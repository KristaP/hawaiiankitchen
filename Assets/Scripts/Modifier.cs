using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class Modifier
{
    //Called every frame, checks if it should interrupt the current recipe
    public virtual bool ContinueRun(Recipe recipe){ return true; }

    public abstract string GetDescription();
    public abstract float Evaluation(Recipe recipe);

    public enum ConditionState
    {
        CorrectIngredience,
        WrongIngredience,
        RecipeFailed,
        RecipeSucceeded
    }
}

[Serializable]
public class Order : Modifier{
    
    public override string GetDescription(){
        return "in order";
    }
    
    public override float Evaluation(Recipe recipe)
    {
        float evaluation = 1f;
        int itemCount = recipe.Ingrediences.Count;

        for(int x = 0; x < itemCount; x++)
            if(recipe.Ingrediences[x] != recipe.GetItemName(x))
                evaluation -= 0.4f;
        
        return evaluation;
    }
}

[Serializable]
public class AnyOrder : Modifier{

    public override string GetDescription(){
        return "in any order";
    }

    public override float Evaluation(Recipe recipe)
    {
        float evaluation = 1f;
        for(int x = 0; x < recipe.Ingrediences.Count; x++)
            if(!recipe.HasToDoItem(recipe.GetItemName(x)))
                evaluation -= 0.4f;

        return evaluation;
    }
}

[Serializable]
public class Timer : AnyOrder
{
    private float startTime;
    private float timeUntilFail;

    public Timer(float length){
        timeUntilFail = length;
        startTime = Time.time;
    }

    public override string GetDescription(){
        return "in " + timeUntilFail + " seconds";
    }

    public override bool ContinueRun(Recipe recipe)
    {
        return Time.time - startTime <= timeUntilFail;
    }
}