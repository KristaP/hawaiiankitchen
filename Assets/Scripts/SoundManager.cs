using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource efxSource;                   //Drag a reference to the audio source which will play the sound effects.
    public AudioSource musicSource;                 //Drag a reference to the audio source which will play the music.
    public static SoundManager instance = null;     //Allows other scripts to call functions from SoundManager.
    public List<AudioClip> music = new List<AudioClip>(){ null, null, null, null, null};             

    private void Awake() {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        GameManager.Instance.OnStateChanged += OnStateChanged;
    }

    void OnStateChanged() {
        if(musicSource.clip != music[(int)GameManager.Instance.State]){
            musicSource.clip = music[(int)GameManager.Instance.State];
            musicSource.Play();
        }
    }
}
