using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class StartUI : MonoBehaviour
{

    public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Plate" && GameManager.Instance.State == GameState.Menu)
        {
            GetComponent<Collider>().enabled = false;
            FoodTable.Instance.SetRecipe(Recipe.GetIngredienceByName("Sandwich"));
            gameObject.SetActive(false);
        }

        if (col.gameObject.tag == "Bin" && GameManager.Instance.State == GameState.Menu)
        {
            #if UNITY_EDITOR
			    UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
    }
}