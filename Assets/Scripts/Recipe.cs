using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.ComponentModel;
using UnityEngine.Events;

[Serializable]
public class Recipe
{
    private static List<Ingredience> _alienBread;
    private static List<Ingredience> _humanBread;

    private static List<Ingredience> _alienIngredience;
    private static List<Ingredience> _humenIngredience;

    private static List<Ingredience> _ingredienceList{
        get{
            return _alienIngredience.Concat(_humenIngredience).Concat(_alienBread).Concat(_humanBread).ToList();
        }
    }

    private static bool _isLoaded;

    public Modifier CurrentModifier;
    private Ingredience _bread;
    public List<Ingredience> Ingrediences = new List<Ingredience>();
    [SerializeField]
    public List<GameObject> ItemsOnSandwich = new List<GameObject>();

    public Recipe(int difficulty, Ingredience bread = null)
    {
        if (!_isLoaded)
            LoadIngredience();

        bool combine = FoodTable.Instance.CombineIngrediences;
        
        //Get Alien/Human food
        bool isAlienFood = (bread == null || combine) && RandomBool();

        List<Ingredience> breadList;
        List<Ingredience> ingredienceList;
        if (!combine)
        {
            breadList = isAlienFood ? _alienBread : _humanBread;
            ingredienceList = isAlienFood ? _alienIngredience : _humenIngredience;
        }
        else
        {
            breadList = _alienBread.Concat(_humanBread).ToList();
            ingredienceList = _alienIngredience.Concat(_humenIngredience).ToList();
        }

        //Generate Random ingredience
        if(bread)
            _bread = bread;
        else
            _bread = RandomElement(breadList);
        Ingrediences.Add(_bread);

        for (int x = 0; x < difficulty; x++)
        {
            Ingredience ingredience = RandomElement(ingredienceList);
            int ingredienceCount = UnityEngine.Random.Range(1, difficulty);
            for(int y = 0; y < ingredienceCount; y++)
                Ingrediences.Add(ingredience);
        }

        //Random Modifier
        switch(UnityEngine.Random.Range(1,3)){
            case 1:
                CurrentModifier = new Timer(UnityEngine.Random.Range(20, 30));
                break;
            case 2:
                CurrentModifier = new Order();
                break;
            case 3:
                CurrentModifier = new AnyOrder();
                break;
        }
    }

    public string GetDescription()
    {
        if(CurrentModifier == null)
            return "";
        string description = "<color=#FFFF00><size=26>" + _bread.Name + "</size></color>\n\n" + CurrentModifier.GetDescription() + " with\n\n";
        string lastIngredience = null;
        int amount = 0;

        //Remove bread
        List<Ingredience> ingrediences = Ingrediences.ToList();
        ingrediences.RemoveAt(0);

        foreach(Ingredience ingredience in ingrediences){
            if(lastIngredience != ingredience){
                if(lastIngredience != null)
                    description += "<color=#FFFF00>" + amount + " " + lastIngredience + "</color>\n";
                amount = 1;
                lastIngredience = ingredience;
            } else
                amount++;
        }
        description += "<color=#FFFF00>" + amount + " " + lastIngredience + "</color>\n";
        return description;
    }

    public static void LoadIngredience()
    {
        _alienBread = Resources.LoadAll<Ingredience>("AlienFood/Bread").ToList();
        _humanBread = Resources.LoadAll<Ingredience>("HumanFood/Bread").ToList();
        _alienIngredience = Resources.LoadAll<Ingredience>("AlienFood/Ingredience").ToList();
        _humenIngredience = Resources.LoadAll<Ingredience>("HumanFood/Ingredience").ToList();
        _isLoaded = true;
    }

    public static Ingredience GetIngredienceByName(string name)
    {
        if (!_isLoaded)
            LoadIngredience();
        return _ingredienceList.Find(x => x.Name == name);
    }

    public static string GetIngredienceNameByMesh(Mesh mesh){
        if (!_isLoaded)
            LoadIngredience();
        foreach(Ingredience ingredience in _ingredienceList){
            if(ingredience.Mesh == mesh)
                return ingredience.Name;
        }
        return "";
    }

    public string GetItemName(int index)
    {
        if(ItemsOnSandwich.Count <= index)
            return "";
        Mesh item = ItemsOnSandwich[index].GetComponent<MeshFilter>().sharedMesh;
        return GetIngredienceNameByMesh(item);
    }

    public bool HasToDoItem(string Item)
    {
        return Ingrediences.Any(x => x.Name == Item);
    }

    public bool AreAllItemsPlaced()
    {
        return Ingrediences.Count == ItemsOnSandwich.Count;
    }

    public float Evaluate()
    {
        return CurrentModifier.Evaluation(this);
    }

    private bool RandomBool()
    {
        return (UnityEngine.Random.value > 0.5f);
    }

    private T RandomElement<T>(List<T> list)
    {
        if (list.Count == 0)
            return default(T);
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static implicit operator string(Recipe i) => i.GetDescription();
}
