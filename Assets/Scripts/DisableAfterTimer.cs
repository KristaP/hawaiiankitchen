using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAfterTimer : MonoBehaviour
{
    public float timer = 10f;

    private void OnEnable() {
        GameManager.Instance.StartCoroutine(DisableAfter());
    }

    IEnumerator DisableAfter() {
        yield return new WaitForSeconds(timer);
        gameObject.SetActive(false);
    }
}
