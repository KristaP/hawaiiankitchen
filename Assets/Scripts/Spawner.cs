using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject Prefab;
    private GameObject spawnedObject;
    
    private void OnEnable()
    {
        if(!spawnedObject)
            spawnedObject = Instantiate(Prefab, transform.position, Quaternion.identity, transform);
    }

    private void OnDisable()
    {
        if(spawnedObject)
            Destroy(spawnedObject);
    }
}
