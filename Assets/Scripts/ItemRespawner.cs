using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRespawner : MonoBehaviour
{
    Vector3 startPosition;
    Quaternion startRotation;
    private Vector3 startScale;
    private Rigidbody rb;
    private Transform parent;
    
    void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startScale = transform.localScale;
        parent = transform.parent;
        rb = GetComponent<Rigidbody>();
    }

    private void OnDisable() {
        if(rb)
            rb.isKinematic = false;
        transform.SetParent(parent, true);
        transform.position = startPosition;
        transform.rotation = startRotation;
        transform.localScale = startScale;
        
    }
}
