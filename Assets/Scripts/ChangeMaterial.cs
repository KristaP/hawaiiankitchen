using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour
{
    private Renderer rend;
    public Texture burnedTexture;

    void burnObject()   //call this to change texture from normal to burned
    {
        rend = this.GetComponent<Renderer>();
        rend.material.mainTexture = burnedTexture;
    }
}
