using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayTrigger : MonoBehaviour
{
    public float Timer;
    public UnityEvent OnTimerFinish;
    
    private void OnEnable()
    {
        StartCoroutine(TimerCoroutine());
    }
    
    private IEnumerator TimerCoroutine()
    {
        yield return new WaitForSeconds(Timer);
        OnTimerFinish.Invoke();
    }
}
