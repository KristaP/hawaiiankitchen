using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "HawaiianKitchen/Ingredient")]
public class Ingredience : ScriptableObject
{
    public string Name;
    public Mesh Mesh;
    public GameObject RoastedPrefab;

    public static implicit operator string(Ingredience i) => i.Name;
}